﻿using UnityEngine;
using System.Collections;

public class MotherShipScript : MonoBehaviour
{
    public bool ShouldGo = false;

    public bool OnDestination;

    public Vector3 DestinationPosition;
    public Vector3 StartingPosition;

    public float TravelTime=1f;
    private float helperTimer = 0;

    float dirx;
    float diry;
    public float speed;

    // Use this for initialization
    void Start()
    {
        StartingPosition = transform.position;
        helperTimer = 0;
        DestinationPosition = Vector2.zero;

        dirx = 1f;
        diry = 1f;
        OnDestination = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.childCount == 0)
        {
            PhaseController.Get.WaveCleared(this);
            Destroy(gameObject);
        }
    }

    void FixedUpdate()
    {
        if (!ShouldGo)
            return;

        if (OnDestination)
        {
            transform.position += new Vector3(dirx * 0.5f, diry * 0.2f, 0f) * Time.deltaTime;
            if (Mathf.Abs(DestinationPosition.y - transform.position.y) > 1)
            {
                diry *= -1f;
            }
            if (Mathf.Abs(DestinationPosition.x - transform.position.x) > 1)
            {
                dirx *= -1f;
            }

        }
        else
        {
            helperTimer += Time.deltaTime;
            transform.position = Vector3.Lerp(StartingPosition, DestinationPosition, helperTimer / TravelTime);

            if (helperTimer >= TravelTime)
            {
                OnDestination = true;
                EnableControllersAndCollidersInChildrens();
            }
        }
    }

    public void EnableControllersAndCollidersInChildrens()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            AlienCotroller alienController = transform.GetChild(i).GetComponent<AlienCotroller>();
            if (alienController != null)
                alienController.enabled = true;

            BoxCollider2D boxCollider = transform.GetChild(i).GetComponent<BoxCollider2D>();
            if (boxCollider != null)
                boxCollider.enabled = true;
        }
    }


}
