﻿using UnityEngine;
using System.Collections;

public class EggController : MonoBehaviour
{
    void Update()
    {
        if (transform.position.y < -10)
            Destroy(gameObject);
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.name == "player")
        {
            PlayerController.Get.GotHit();
        }

        Instantiate(PrefabsHelper.Get.EggDestroyParticlePrefab, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
