﻿using UnityEngine;
using System.Collections;

public class PowerUpController : MonoBehaviour
{

    private float diry = 1f;
    private float startY;

    // Use this for initialization
    void Start()
    {
        startY = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(2f, diry * 0.4f, 0f) * Time.deltaTime;
        if (Mathf.Abs(startY - transform.position.y) > .5f)
        {
            diry *= -1f;
        }

    }
}
