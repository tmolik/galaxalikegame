﻿using UnityEngine;
using System.Collections;

public class Boss1Script : MonoBehaviour
{

    public static Boss1Script Get { get; private set; }

    public Boss1Script()
    {
        Get = this;
    }

    public GameObject Part1Object;
    public GameObject Part2Object;
    public GameObject Part3Object;
    public GameObject Part4Object;

    private int part1hp = 4;
    private int part2hp = 4;
    private int part3hp = 4;
    private int part4hp = 5;

    public void Hit(string part)
    {
        switch (part)
        {
            case "Part1":
                {
                    if ((Part2Object==null) && (Part3Object==null))
                    {
                        part1hp--;
                        if (part1hp == 0)
                        {
                            Destroy(Part1Object);
                        }
                    }
                    break;
                }
            case "Part2":
                {
                    part2hp--;
                    if (part2hp == 0)
                    {
                        Destroy(Part2Object);
                    }
                    break;
                }
            case "Part3":
                {
                    part3hp--;
                    if (part3hp == 0)
                    {
                        Destroy(Part3Object);
                    }
                    break;
                }
            case "Part4":
                {
                    if ((Part1Object==null) && (Part2Object == null) && (Part3Object == null))
                    {
                        part4hp--;
                        if (part4hp == 0)
                        {
                            Destroy(Part4Object);
                        }

                        Instantiate(PrefabsHelper.Get.BossDestroyParticle, transform.position, transform.rotation);
                        Instantiate(PrefabsHelper.Get.BossDestroyParticle, transform.position + Vector3.up * 1f, transform.rotation);
                        Instantiate(PrefabsHelper.Get.BossDestroyParticle, transform.position + Vector3.left * 1f, transform.rotation);
                        Destroy(gameObject);
                    }
                    break;
                }

        }

    }
}
