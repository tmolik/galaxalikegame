﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PhaseController : MonoBehaviour
{
    public static PhaseController Get { get; private set; }

    public PhaseController()
    {
        Get = this;
    }


    private float lastPowerUpTime;
    public GameObject powerUpPrefab;

    public List<MotherShipScript> MotherShips = new List<MotherShipScript>();

    // Use this for initialization
    void Start()
    {
        lastPowerUpTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - lastPowerUpTime > Random.Range(10, 16))
        {
            newPowerUp();
        }
    }

    public void WaveCleared(MotherShipScript wave)
    {
        MotherShips.Remove(wave);
        if (MotherShips.Count > 0)
            MotherShips[0].ShouldGo = true;
    }

    private void newPowerUp()
    {
        GameObject powerup = Instantiate(PrefabsHelper.Get.PowerUpPrefab, new Vector3(-12f, -1f, 0), transform.rotation);
        powerup.transform.SetParent(PrefabsHelper.Get.PowerUpsTransform);
        lastPowerUpTime = Time.time;
    }
}
