﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabsHelper : MonoBehaviour
{
    public static PrefabsHelper Get { get; private set; }

    public PrefabsHelper()
    {
        Get = this;
    }

    public GameObject PowerUpPrefab;

    public GameObject PlayerBeamPrefab;

    public GameObject BeamDestroyParticlePrefab;

    public GameObject PowerUpDestroyParticlePrefab;

    public GameObject EggDestroyParticlePrefab;

    public GameObject EggPrefab;

    public GameObject AlienBeamPrefab;

    public GameObject BossDestroyParticle;

    public Transform PowerUpsTransform;
}
