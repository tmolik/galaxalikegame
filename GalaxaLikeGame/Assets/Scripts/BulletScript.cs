﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour
{
    /// <summary>
    /// Was this bullet fired by player
    /// </summary>
    public bool FiredByPlayer = true;

    void Update()
    {
        transform.position += new Vector3(0, FiredByPlayer ? 10 : -10, 0) * Time.deltaTime;

        if ((transform.position.y > 5.5f) || (transform.position.y < -10))
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if ((coll.gameObject.name == "player") && (FiredByPlayer))
            return;

        if ((coll.gameObject.name == "player") && (!FiredByPlayer))
        {
            GameObject.Find("player").GetComponent<PlayerController>().GotHit();
            Destroy(gameObject);
            return;
        }

        Instantiate(PrefabsHelper.Get.BeamDestroyParticlePrefab, coll.gameObject.transform.position, transform.rotation);
        Destroy(gameObject);
        Destroy(coll.gameObject);

    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (FiredByPlayer)
        {
            if (coll.tag == "boss")
            {
                Destroy(gameObject);
                Boss1Script.Get.Hit(coll.name);
            }
            if (coll.tag == "pup")
            {
                Instantiate(PrefabsHelper.Get.PowerUpDestroyParticlePrefab, coll.gameObject.transform.position, transform.rotation);
                Destroy(gameObject);
                Destroy(coll.gameObject);

                PlayerController.Get.GetNewWeapon();
            }
        }
    }
}
