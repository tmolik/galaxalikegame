﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Get { get; private set; }

    public PlayerController()
    {
        Get = this;
    }

    private float speed = 6.0f;

    private float lastShotTime;

    private int weapon;

    private float hitTime;
    private float migTime;
    private bool hit;
    private int lives;

    // Use this for initialization
    void Start()
    {
        weapon = 1;
        lives = 3;
    }

    void Update()
    {
        float translationH = Input.GetAxis("Horizontal") * speed;
        transform.Translate(translationH * Time.deltaTime, 0, 0);

        if (Input.GetKey(KeyCode.LeftControl))
        {
            if (Time.time - lastShotTime > 0.5f)
                Shot();
        }

        if (transform.position.x > 11.5f)
            transform.position = new Vector3(-11f, transform.position.y);
        if (transform.position.x < -11.5f)
            transform.position = new Vector3(11f, transform.position.y);
    }

    void FixedUpdate()
    {
        if (Time.time - hitTime > 2f)
        {
            GetComponent<BoxCollider2D>().enabled = true;
            GetComponent<SpriteRenderer>().enabled = true;
            hit = false;
        }
        if (hit)
        {
            if (Time.time - migTime > .1f)
            {
                GetComponent<SpriteRenderer>().enabled = !GetComponent<SpriteRenderer>().enabled;
                migTime = Time.time;
            }
        }

    }

    private void Shot()
    {
        if (weapon == 1)
        {
            Instantiate(PrefabsHelper.Get.PlayerBeamPrefab, new Vector3(transform.position.x + 0.07f, transform.position.y + 2f, transform.position.z), transform.rotation);
        }
        if (weapon == 2)
        {
            Instantiate(PrefabsHelper.Get.PlayerBeamPrefab, new Vector3(transform.position.x - 1.1f, transform.position.y + 0.9f, transform.position.z), transform.rotation);
            Instantiate(PrefabsHelper.Get.PlayerBeamPrefab, new Vector3(transform.position.x + 1.2f, transform.position.y + 0.9f, transform.position.z), transform.rotation);

        }
        if (weapon == 3)
        {
            Instantiate(PrefabsHelper.Get.PlayerBeamPrefab, new Vector3(transform.position.x + 0.07f, transform.position.y + 2f, transform.position.z), transform.rotation);
            Instantiate(PrefabsHelper.Get.PlayerBeamPrefab, new Vector3(transform.position.x - 1.1f, transform.position.y + 0.9f, transform.position.z), transform.rotation);
            Instantiate(PrefabsHelper.Get.PlayerBeamPrefab, new Vector3(transform.position.x + 1.2f, transform.position.y + 0.9f, transform.position.z), transform.rotation);
        }

        lastShotTime = Time.time;
    }

    public void GetNewWeapon()
    {
        weapon = Random.Range(1, 4);
    }

    public void GotHit()
    {
        hitTime = Time.time;
        GetComponent<BoxCollider2D>().enabled = false;
        hit = true;
        migTime = hitTime;
        lives--;
    }


}
