﻿using UnityEngine;
using System.Collections;

public class AlienCotroller : MonoBehaviour
{
    public bool dropsEgg;
    public bool shoots;

    // Update is called once per frame
    void Update()
    {
        if (dropsEgg)
        {
            if (Random.Range(0, 1000) > 990)
                DropEgg();
        }

        if (shoots)
        {
            if (Random.Range(0, 1000) > 980)
                Shoot();
        }

    }

    void DropEgg()
    {
        Instantiate(PrefabsHelper.Get.EggPrefab, transform.position + Vector3.down * 1.2f, transform.rotation);
    }

    void Shoot()
    {
        GameObject beam = Instantiate(PrefabsHelper.Get.AlienBeamPrefab, transform.position + Vector3.down * 1.1f, transform.rotation) as GameObject;
        beam.GetComponent<BulletScript>().FiredByPlayer = false;
    }
}
