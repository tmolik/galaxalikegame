﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyParticle : MonoBehaviour
{
    public float DestroyTime=5;

    private float helperTimer;

    // Update is called once per frame
    void Update()
    {
        helperTimer += Time.deltaTime;
        if(helperTimer >= DestroyTime)
        {
            Destroy(gameObject);
        }
    }
}
